var Toolbox = function () {
    var name = 'CCS Toolbox',
        optionsData,

        view = {
            main: {
                getHtml: function () {
                    var tempHtml = '';
                    tempHtml += '<header>' +
                        '<h1>Configure logging</h1>' +
                        '<p>No reload required.</p>' +
                        '</header>';
                    tempHtml += '<div id="fcLoggerExtension">';
                    tempHtml += '<section id="loggerSelector">' + view.loggerSelector.getHtml() + '</section>';
                    tempHtml += '<section id="settingsContainer">' + view.settingsContainer.getHtml() + '</section>';
                    tempHtml += '</div>';
                    tempHtml += '<footer>' + view.footer.getHtml() + '</footer>';

                    return tempHtml;
                },

                setBehavior: function () {
                    view.loggerSelector.setBehavior();
                    view.settingsContainer.setBehavior();
                    view.footer.setBehavior();

                    //$('#submitDialog').on('click', action.submitData)

                }
            },

            footer: {
                getHtml: function () {
                    var tempHtml = '';
                    tempHtml += '<span id="saveSettings" class="button">Ok</span>';
                    tempHtml += '<span id="setDefault" class="button">Default</span>';
                    return tempHtml
                },

                setBehavior: function () {
                    $('#saveSettings').on('click', action.refreshLogger);
                    $('#setDefault').on('click', action.setDefault);

                }
            },

            loggerSelector: {
                getHtml: function () {
                    function getCustomLoggerHtml(logger, options) {
                        var tempHtml = '',
                            checked;
                        checked = (options && options.enabled) ? 'checked="checked"' : '';
                        tempHtml += '<li data-id="' + logger + '">';
                        tempHtml += '<input type="checkbox" data-id="' + logger + '" ' + checked + '>';
                        tempHtml += '<label>' + logger + '</label>';
                        tempHtml += '</li>';
                        return tempHtml;
                    }

                    var tempHtml = '';
                    tempHtml += '<ul id="loggerSelectorList">';
                    tempHtml += getCustomLoggerHtml('globalOptions', optionsData.globalOptions);
                    //tempHtml += getCustomLoggerHtml('defaultOptions', optionsData.defaultOptions);
                    for (var logger in optionsData) {
                        if (optionsData.hasOwnProperty(logger) &&
                            logger != 'defaultOptions' &&
                            logger != 'globalOptions') {
                            console.log('loggerSelector-getHtml:', logger);
                            tempHtml += getCustomLoggerHtml(logger, optionsData[logger]);
                        }
                    }
                    tempHtml += '</ul>';
                    return tempHtml
                },

                setBehavior: function () {
                    var loggerSelectorList = $('#loggerSelectorList');

                    action.changeConfig('globalOptions');
                    loggerSelectorList.find('li').on('click change', function () {
                        var clickedEl = $(this);
                        var id = clickedEl.attr('data-id');
                        $('#loggerSelectorList').find('> li').removeClass('active');
                        clickedEl.addClass('active');
                        action.changeConfig(id);
                    });

                    loggerSelectorList.find('li').first().find('input').on('change', function (e) {
                        if ($(this).is(':checked')) {
                            loggerSelectorList.find('li').show();
                            action.changeConfig('globalOptions');
                        } else {
                            loggerSelectorList.find('li').hide();
                            $(this).parent().show();
                            action.changeConfig('notEnabled');
                        }
                        e.stopPropagation();
                    }).trigger('change');
                }
            },

            settingsContainer: {
                getHtml: function () {
                    var tempHtml = '';
                    tempHtml += '<div id="notEnabled"><p>Logging disabled</p></div>';
                    tempHtml += '<div id="globalOptions" class="noBorder" data-id="globalOptions">' + view.globalSettings.getHtml() + '</div>';
                    tempHtml += '<div id="defaultOptions" class="noBorder">' + view.loggerSettings.getHtml('defaultOptions') + '</div>';
                    for (var logger in optionsData) {
                        if (optionsData.hasOwnProperty(logger) &&
                            logger != 'defaultOptions' &&
                            logger != 'globalOptions') {
                            tempHtml += '<div id="' + logger + '" class="loggerSetting noBorder">';
                            tempHtml += view.loggerSettings.getHtml(logger);
                            tempHtml += '</div>';
                        }
                    }
                    return tempHtml
                },

                setBehavior: function () {
                    view.globalSettings.setBehavior();
                    for (var logger in optionsData) {
                        if (optionsData.hasOwnProperty(logger)) {
                            view.loggerSettings.setBehavior(logger);
                        }
                    }
                }
            },

            globalSettings: {
                getHtml: function () {
                    var tempHtml = '';

                    function getLogItemHtml(name, text, subItem, checked) {
                        var tempHtml = '',
                            checkedString = (checked) ? 'checked="checked"' : '',
                            classString = (subItem) ? 'checkBoxContainer subItem' : 'checkBoxContainer';
                        tempHtml += '<li>';
                        tempHtml += '   <div class="' + classString + '">';
                        tempHtml += '       <label><input type="checkbox" name="' + name + '" ' + checkedString + '>';
                        tempHtml += '       ' + text + '</label>';
                        tempHtml += '   </div>';
                        tempHtml += '</li>';
                        return tempHtml;
                    }

                    tempHtml += '<h2 class="center">Global Settings</h2>';
                    tempHtml += '<ul id="LogSettingsList">';
                    tempHtml += getLogItemHtml('error', 'Error function', false, optionsData.globalOptions.error);
                    tempHtml += getLogItemHtml('warn', 'Warn function', false, optionsData.globalOptions.warn);
                    tempHtml += getLogItemHtml('trace', 'Trace function', false, optionsData.globalOptions.trace);
                    tempHtml += getLogItemHtml('info', 'Info function', false, optionsData.globalOptions.info);
                    tempHtml += getLogItemHtml('log', 'Log function', false, optionsData.globalOptions.log);
                    tempHtml += '</ul>';
                    tempHtml += '';
                    return tempHtml
                },

                setBehavior: function () {
                    $('#SelectAllLogger').on('click', action.selectAll)
                }
            },

            loggerSettings: {
                getHtml: function (logger) {
                    function getLogItemHtml(name, text, subItem, enabled) {
                        var tempHtml = '',
                            checked = enabled ? 'checked="checked"' : '',
                            classString = (subItem) ? 'checkBoxContainer subItem' : 'checkBoxContainer';
                        tempHtml += '<li>';
                        tempHtml += '   <div class="' + classString + '">';
                        tempHtml += '       <input type="checkbox" name="' + name + '" ' + checked + '>';
                        tempHtml += '       <label>' + text + '</label>';
                        tempHtml += '   </div>';
                        tempHtml += '</li>';
                        return tempHtml;
                    }

                    function getLogItemColorHtml(name, text, subItem, color) {
                        var tempHtml = '',
                            color = color;
                        classString = (subItem) ? 'checkBoxContainer subItem' : 'checkBoxContainer';

                        if (!color) {
                            switch (name) {
                                case 'fontColor' :
                                    color = '#000000';
                                    break;
                                default :
                                    color = '#FFFFFF';
                            }
                        }
                        tempHtml += '<li>';
                        tempHtml += '   <div class="' + classString + '">';
                        tempHtml += '       <input type="color" name="' + name + '" value="' + color + '">';
                        tempHtml += '       <label>' + text + '</label>';
                        tempHtml += '   </div>';
                        tempHtml += '</li>';
                        return tempHtml;
                    }

                    var tempHtml = '',
                        options = optionsData[logger];
                    tempHtml += '<h2 class="center">' + logger + ' Logger</h2>';
                    tempHtml += '<ul id="LogSettingsList">';
                    console.log('logger : ', options);
                    tempHtml += getLogItemColorHtml('fontColor', 'Font color', false, options.fontColor);
                    tempHtml += getLogItemColorHtml('backgroundColor', 'Background color', false, options.backgroundColor);
                    tempHtml += getLogItemHtml('useGlobal', 'Use global settings', false, options.useGlobal);
                    tempHtml += getLogItemHtml('error', 'Error function', true, options.error);
                    tempHtml += getLogItemHtml('warn', 'Warn function', true, options.warn);
                    tempHtml += getLogItemHtml('trace', 'Trace function', true, options.trace);
                    tempHtml += getLogItemHtml('info', 'Info function', true, options.info);
                    tempHtml += getLogItemHtml('log', 'Log function', true, options.log);
                    tempHtml += '</ul>';
                    tempHtml += '';
                    return tempHtml
                },

                setBehavior: function (logger) {
                    var loggerContainer = $('#' + logger);

                    loggerContainer.find('input[name="useGlobal"]').on('change', function () {
                        if ($(this).is(':checked')) {
                            loggerContainer.find('.subItem input').attr('disabled', 'disabled');
                        } else {
                            loggerContainer.find('.subItem input').removeAttr('disabled');
                        }
                    }).trigger('change');
                }
            }
        },

        action = {
            parseData: function () {
                var tempData = {},
                    parseDiv = function (divId) {
                        var inputElements,
                            returnObject = {};
                        returnObject['enabled'] = $('#loggerSelectorList').find('input[data-id="' + divId + '"]').is(':checked');
                        inputElements = $('#' + divId).find('input[type="checkbox"]');
                        for (var i = 0; i < inputElements.length; i++) {
                            var name = $(inputElements[i]).attr('name');
                            returnObject[name] = $(inputElements[i]).is(':checked');
                        }

                        inputElements = $('#' + divId).find('input[type="color"]');
                        for (var i = 0; i < inputElements.length; i++) {
                            var name = $(inputElements[i]).attr('name');
                            returnObject[name] = $(inputElements[i]).val();
                        }

                        console.log('parseDivData: ', returnObject);
                        return returnObject;
                    };

                for (var logger in optionsData) {
                    if (optionsData.hasOwnProperty(logger)) {
                        tempData[logger] = parseDiv(logger)
                    }
                }
                return tempData;
            },

            render: function () {
                var body = document.body.innerHTML = view.main.getHtml();
                view.main.setBehavior();
            },

            loadData: function () {
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {action: "getLoggerDataObject"}, function (response) {
                        console.log('loadDataResponse: ', response);
                        optionsData = response;
                        action.render();
                    });
                });
            },

            refreshLogger: function () {
                var data = action.parseData();
                console.log('refreshlogger', data);
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {
                        action: "refreshLogger",
                        data: data
                    }, function (response) {
                    });
                });
            },

            setDefault: function () {
                console.log('setDefault');
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, {
                        action: "setDefault"
                    }, function (response) {
                    });
                });
            },

            changeConfig: function (id) {
                $('#settingsContainer').find(' > div').hide();
                $('#' + id).show();
            }
        };

    return {
        init: function () {
            action.loadData();
        }
    }
}();

document.addEventListener('DOMContentLoaded', function () {
    Toolbox.init();
});
