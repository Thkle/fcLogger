//noinspection JSUnresolvedVariable
/*chrome.extension.onMessage.addListener(function (msg) {
 if (msg.action) {
 switch (msg.action) {
 case 'getLoggerDataObject' :
 var loggerDataObject = {
 defaultSettings : 'jo',
 loggerList : 'muha'
 };
 document.documentElement.dispatchEvent(new CustomEvent('getDefaultLoggerSettings'));
 break;
 case 'refreshLogger' :
 document.documentElement.dispatchEvent(new CustomEvent('getLoggerObjects'));
 break;
 }
 }
 });*/

var loggerInjection = function () {
    var actions = {
        init: function () {
            chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {
                if (request.action) {
                    switch (request.action) {
                        case 'getLoggerDataObject' :
                            sendResponse(actions.getLoggerSettings());
                            break;
                        case 'refreshLogger' :
                            actions.refreshLogger(request.data);
                            break;
                        case 'setDefault' :
                            actions.setDefault();
                            break;
                        default:
                            console.log('unknown Action: ', request.action)
                    }
                }
            });
        },

        refreshLogger: function (data) {
            console.log('refreshLogger', data);
            localStorage.setItem('fcLoggerData', JSON.stringify(data));
            document.documentElement.dispatchEvent(new CustomEvent('refreshLogger'));
        },

        setDefault: function () {
            console.log('setDefault');
            localStorage.removeItem('fcLoggerData');
            location.reload(true);
        },

        getLoggerSettings: function () {
            var tempData;
            try {
                tempData = JSON.parse(localStorage.getItem('fcLoggerData'));
                if (!(tempData.defaultOptions && tempData.globalOptions)) {
                    console.warn('fcLoggerData default and global not set');
                }
            } catch (e) {
                console.warn('fcLoggerData not set');
                tempData = {};
            }
            return tempData;
        }
    };

    actions.init();

}();

/*
 chrome.runtime.onMessage.addListener(
 function(request, sender, sendResponse) {
 console.log(sender.tab ?
 "from a content script:" + sender.tab.url :
 "from the extension");
 var loggerDataObject = {
 defaultSettings : 'jo',
 loggerList : 'muha'
 };

 if (request.greeting == "hello")
 sendResponse({farewell: "goodbye"});
 }
 );*/
