//noinspection JSUnresolvedVariable
chrome.extension.onMessage.addListener(function (msg) {
    if (msg.action) {
        switch (msg.action) {
            case 'openConfigDialog' :
                document.documentElement.dispatchEvent(new CustomEvent('openConfigDialog'));
                break;
            case 'openLoggerDialog' :
                document.documentElement.dispatchEvent(new CustomEvent('openLoggerDialog'));
                break;
            case 'openJiraIssue' :
                document.documentElement.dispatchEvent(new CustomEvent('openJiraIssue'));
                break;
            case 'showTestDialog':
                document.documentElement.dispatchEvent(new CustomEvent('showTestDialog'));
                break;
        }
    }
});