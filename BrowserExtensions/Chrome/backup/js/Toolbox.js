var Toolbox = function () {
    var name = 'CCS Toolbox',
        view = {
            main: {
                getHtml: function () {
                    var tempHtml = '';
                    tempHtml += '<div id="ToolboxContainer">';
                    tempHtml += '<h1>' + name + '</h1>';
                    tempHtml += '<ul>';
                    tempHtml += '<li id="showConfigDialog">';
                    tempHtml += '   <h2>Config</h2>';
                    tempHtml += '</li>';
                    tempHtml += '<li id="showLoggerDialog">';
                    tempHtml += '   <h2>Logger</h2>';
                    tempHtml += '</li>';
                    tempHtml += '<li id="openJiraIssue">';
                    tempHtml += '   <h2>New Issue</h2>';
                    tempHtml += '</li>';
                    tempHtml += '<li id="showTestDialog">';
                    tempHtml += '   <h2>Tests</h2>';
                    tempHtml += '</li>';
                    tempHtml += '</ul>';
                    tempHtml += '</div>';
                    return tempHtml;
                },

                setBehavior: function () {
                    var tempEl;
                    tempEl = document.getElementById('showConfigDialog');
                    tempEl.addEventListener('click', action.openConfigDialog);

                    tempEl = document.getElementById('showLoggerDialog');
                    tempEl.addEventListener('click', action.openLoggerDialog);

                    tempEl = document.getElementById('openJiraIssue');
                    tempEl.addEventListener('click', action.openJiraIssue);

                    tempEl = document.getElementById('showTestDialog');
                    tempEl.addEventListener('click', action.showTestDialog);
                }
            }
        },

        action = {
            openConfigDialog: function () {
                //noinspection JSUnresolvedVariable
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    //noinspection JSUnresolvedVariable
                    chrome.tabs.sendMessage(tabs[0].id, {action: "openConfigDialog"}, function (response) {
                    });
                });
            },

            openLoggerDialog: function () {
                //noinspection JSUnresolvedVariable
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    //noinspection JSUnresolvedVariable
                    chrome.tabs.sendMessage(tabs[0].id, {action: "openLoggerDialog"}, function (response) {
                    });
                });
            },

            openJiraIssue: function () {
                //noinspection JSUnresolvedVariable
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    //noinspection JSUnresolvedVariable
                    chrome.tabs.sendMessage(tabs[0].id, {action: "openJiraIssue"}, function (response) {
                    });
                });
            },

            showTestDialog: function () {
                //noinspection JSUnresolvedVariable
                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                    //noinspection JSUnresolvedVariable
                    chrome.tabs.sendMessage(tabs[0].id, {action: "showTestDialog"}, function (response) {
                    });
                });
            }
        };

    return {
        init: function () {
            console.log('test');
            var body = document.body.innerHTML = view.main.getHtml();
            view.main.setBehavior();
            var port = chrome.extension.connect({name: "Sample Communication"});
            port.postMessage("Hi BackGround");
            port.onMessage.addListener(function (msg) {
                console.log("message recieved" + msg);
            });
        }
    }
}();

document.addEventListener('DOMContentLoaded', function () {
    Toolbox.init();
});

