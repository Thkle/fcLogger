LoggerCtrl = function () {
    var loggerListObject = {},
        optionsObject = {
            globalOptions: {
                enabled: true,
                error: true,
                warn: true,
                trace: false,
                info: false,
                log: false
            },
            defaultOptions: {
                enabled: true,
                useGlobal: true,
                error: true,
                warn: true,
                trace: true,
                info: true,
                log: true
            }
        },
        zeroFunction = function () {
        },
        actions = {
            addLogger: function (namespace, logger) {
                loggerListObject[namespace] = logger;
                optionsObject[namespace] = logger.options;
                actions.saveLoggerOptions();
            },

            refreshAllLogger: function () {
                for (var tempLogger in loggerListObject) {
                    if (loggerListObject.hasOwnProperty(tempLogger)) {
                        loggerListObject[tempLogger].refresh();
                    }
                }
            },

            constructLogger: function (namespace, tempOptions) {
                var logger = {
                    namespace: namespace,
                    options: tempOptions
                };

                logger.init = function () {
                    for (var m in console)
                        //noinspection JSUnfilteredForInLoop
                        if (typeof console[m] === 'function') {
                            if (logger.options.enabled && optionsObject.globalOptions.enabled) {
                                if (logger.options.useGlobal) {
                                    //noinspection JSUnfilteredForInLoop
                                    (optionsObject.globalOptions[m]) ? logger[m] = console[m].bind(window.console, namespace + ": ") : logger[m] = zeroFunction
                                } else {
                                    var backgroundColor = logger.options['backgroundColor'] || '#ffffff';
                                    var fontColor = logger.options['fontColor'] || '#000000';
                                    //noinspection JSUnfilteredForInLoop
                                    (logger.options[m]) ? logger[m] = console[m].bind(window.console, '%c' + namespace + ": ", 'background: ' + backgroundColor + '; color: ' + fontColor) : logger[m] = zeroFunction
                                }
                            } else {
                                //noinspection JSUnfilteredForInLoop
                                logger[m] = zeroFunction
                            }
                        }
                };
                logger.refresh = function (options, persistent) {
                    logger.options = options || defaultOptions;
                    optionsObject[logger.namespace] = logger.options;
                    if (persistent) {
                        optionsObject[logger.namespace] = logger.options;
                        actions.saveLoggerOptions();
                    }
                    logger.init();
                };
                logger.init();
                actions.addLogger(namespace, logger);
                return logger
            },

            saveLoggerOptions: function () {
                localStorage.setItem('fcLoggerData', JSON.stringify(optionsObject));
            },

            loadLoggerOptions: function () {
                var tempData;
                try {
                    tempData = JSON.parse(localStorage.getItem('fcLoggerData'));
                    if (!(tempData.defaultOptions && tempData.globalOptions)) {
                        tempData = optionsObject;
                    }
                } catch (e) {
                    console.warn('fcLoggerData not set');
                    tempData = optionsObject;
                }
                optionsObject = tempData;
            },

            listenOnEvents: function () {
                document.documentElement.addEventListener('refreshAllLogger', function () {
                    actions.refreshAllLogger();
                });
            }
        };

    actions.listenOnEvents();
    actions.loadLoggerOptions();

    return {
        getLogger: function (namespace, options) {
            if (!window.console) return {
                error: zeroFunction,
                warn: zeroFunction,
                trace: zeroFunction,
                info: zeroFunction,
                log: zeroFunction
            };

            var tempNamespace = namespace || '',
                tempOptions;

            if (!tempNamespace) {
                console.warn('no namespace set for logger');
            }

            if (typeof optionsObject[namespace] == 'object') {
                tempOptions = optionsObject[namespace];
            } else {
                if (typeof options === 'object') {
                    tempOptions = options;
                    tempOptions.enabled = true;
                    tempOptions.useGlobal = tempOptions.useGlobal || false;
                } else {
                    tempOptions = optionsObject.defaultOptions
                }
            }
            return actions.constructLogger(tempNamespace, tempOptions);
        }
    }
}();